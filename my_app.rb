
class MyApp
  def initialize
    @view = ->(env) {env.keys.sort.map {|key| "#{bold {key}} = #{env[key]}<br />"}}
  end
  def call env
    [200, {"Content-Type" => "text/html"}, @view.call(env)]
  end
  def bold
    "<b>#{yield} </b>"
  end
end

